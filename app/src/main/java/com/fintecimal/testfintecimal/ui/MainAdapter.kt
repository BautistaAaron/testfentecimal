package com.fintecimal.testfintecimal.ui

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fintecimal.testfintecimal.R
import com.fintecimal.testfintecimal.base.BaseViewHolder
import com.fintecimal.testfintecimal.data.model.placeEntity
import kotlinx.android.synthetic.main.item_list.view.*


class MainAdapter(
    private val context: Context, private val list: List<placeEntity>,
    private val itemClickListener: OnClickListener
) : RecyclerView.Adapter<BaseViewHolder<*>>() {

    interface OnClickListener {
        fun onClick(place: placeEntity, position: Int)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        return MainViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_list, parent, false)
        )

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        when (holder) {
            is MainViewHolder -> holder.bind(list[position], position)
        }
    }

    inner class MainViewHolder(itemView: View) : BaseViewHolder<placeEntity>(itemView) {
        override fun bind(item: placeEntity, position: Int) {
            if (!item.visited) {
                itemView.txt_visited.text = "Pendiente"
                itemView.txt_visited.setTextColor(Color.parseColor("#AAAAAA"))
                itemView.cardCircle.setCardBackgroundColor(Color.parseColor("#AAAAAA"))
            } else {
                itemView.txt_visited.text  = "Visitada"
                itemView.txt_visited.setTextColor(Color.parseColor("#3ac2c2"))
                itemView.cardCircle.setCardBackgroundColor(Color.parseColor("#3ac2c2"))
            }
            itemView.txt_name_stree.text = item.streetName
            itemView.txt_suburb.text = item.suburb
            itemView.setOnClickListener {
                itemClickListener.onClick(item, position)
            }
        }

    }


}