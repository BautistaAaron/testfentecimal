package com.fintecimal.testfintecimal.ui.view

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fintecimal.testfintecimal.AppDataBase
import com.fintecimal.testfintecimal.R
import com.fintecimal.testfintecimal.data.DataSourceImpl
import com.fintecimal.testfintecimal.data.model.placeEntity
import com.fintecimal.testfintecimal.domain.RepoImpl
import com.fintecimal.testfintecimal.ui.MainAdapter
import com.fintecimal.testfintecimal.ui.viewmodel.MainViewModel
import com.fintecimal.testfintecimal.ui.viewmodel.VMFactory
import com.fintecimal.testfintecimal.vo.Resource
import kotlinx.android.synthetic.main.fragment_main.*


class MainFragment : Fragment(), MainAdapter.OnClickListener {

    private lateinit var adapter: MainAdapter
    private lateinit var recyclerView: RecyclerView
    private lateinit var progress: ProgressBar

    private val viewModel by viewModels<MainViewModel> {
        VMFactory(
            RepoImpl(
                DataSourceImpl(
                    AppDataBase.getDataBase(requireContext())
                )
            )
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView = view.findViewById(R.id.rv_places)
        progress = view.findViewById(R.id.progressBarMainFragment)
        setUpObservers()

    }

    private fun setUpObservers() {
        viewModel.getAllPlaces().observe(requireActivity(), Observer { result ->
            when (result) {
                is Resource.Loading -> {
                    progress.visibility = View.VISIBLE
                }
                is Resource.Success -> {
                    val sizeList = result.data.size
                    val sizePref = getSizePrefer()

                    if (sizePref == 0) {
                        saveSizeListPreference(sizeList)
                        val list = result.data.map {
                            placeEntity(
                                0,
                                it.streetName,
                                it.suburb,
                                it.visited,
                                it.location.latitude,
                                it.location.longitude
                            )
                        }

                        savePlaces(list)
                    } else {
                        setUpListRoom()
                    }
                }
                is Resource.Failure -> {
                    progress.visibility = View.INVISIBLE
                    Toast.makeText(
                        requireContext(),
                        "Error al cargar los datos",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        })
    }

    private fun getSizePrefer(): Int {
        val sharedPreference =
            requireContext().getSharedPreferences(getString(R.string.prefer), Context.MODE_PRIVATE)
        return sharedPreference.getInt(getString(R.string.KEY_SIZE), 0)
    }

    private fun saveSizeListPreference(size: Int) {
        val sharedPreference =
            requireContext().getSharedPreferences(getString(R.string.prefer), Context.MODE_PRIVATE)
                ?: return
        with(sharedPreference.edit()) {
            putInt(getString(R.string.KEY_SIZE), size)
            commit()
        }
    }

    private fun savePlaces(list: List<placeEntity>) {
        viewModel.savePlaces(list).observe(requireActivity(), Observer { result ->
            when (result) {
                is Resource.Loading -> {
                    progress.visibility = View.VISIBLE
                }
                is Resource.Success -> {
                    setUpListRoom()
                }
                is Resource.Failure -> {
                    Toast.makeText(requireContext(), "Faild", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun setUpListRoom() {
        viewModel.getAllPlacesRoom().observe(requireActivity(), Observer { result ->
            when (result) {
                is Resource.Loading -> {
                    progress.visibility = View.VISIBLE
                }
                is Resource.Success -> {
                    var sizeVisitToDo = 0
                    val sizeList = result.data.size
                    result.data.map { if (it.visited) { sizeVisitToDo++ } }

                    if(sizeVisitToDo ==sizeList){
                        progress.visibility = View.INVISIBLE
                        putCardAllPlaceVisited()
                    }else{
                        setAdapterPlacesAndQuantityVisitToDo(result.data)
                        setQuantityVisitsToDo(sizeList-sizeVisitToDo)
                    }
                }
                is Resource.Failure -> {
                    progress.visibility = View.INVISIBLE
                    Toast.makeText(requireContext(), "Faild", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

   private fun putCardAllPlaceVisited(){
        recyclerView.visibility = View.INVISIBLE
       card_all_visits_done.visibility = View.VISIBLE
   }

    private fun setQuantityVisitsToDo(quantityListToDo: Int) {
        txt_visits_to_do.text = "Tienes ${quantityListToDo} visitas por hacer"
    }

    private fun setAdapterPlacesAndQuantityVisitToDo(place: List<placeEntity>) {
        adapter = MainAdapter(requireContext(), place, this)
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.adapter = adapter
        progress.visibility = View.INVISIBLE
    }


    override fun onClick(place: placeEntity, position: Int) {
        val bundle = Bundle()
        bundle.putParcelable("place", place)
        findNavController().navigate(R.id.action_mainFragment_to_mapsFragment, bundle)
    }
}