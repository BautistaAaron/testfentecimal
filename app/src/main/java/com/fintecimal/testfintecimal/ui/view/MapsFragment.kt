package com.fintecimal.testfintecimal.ui.view

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import androidx.fragment.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.fintecimal.testfintecimal.AppDataBase
import com.fintecimal.testfintecimal.R
import com.fintecimal.testfintecimal.data.DataSourceImpl
import com.fintecimal.testfintecimal.data.model.placeEntity
import com.fintecimal.testfintecimal.domain.RepoImpl
import com.fintecimal.testfintecimal.ui.viewmodel.MainViewModel
import com.fintecimal.testfintecimal.ui.viewmodel.VMFactory
import com.fintecimal.testfintecimal.vo.Resource
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.fragment_maps.*

class MapsFragment : Fragment() {
    private lateinit var place: placeEntity
    private lateinit var map: GoogleMap
    private val viewModel by viewModels<MainViewModel> {
        VMFactory(
            RepoImpl(
                DataSourceImpl(
                    AppDataBase.getDataBase(requireContext())
                )
            )
        )
    }
    private val callback = OnMapReadyCallback { googleMap -> }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireArguments().let {
            place = it.getParcelable("place")!!
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_maps, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync {
            setUtils(view, place, it)
        }

        btn_visited.setOnClickListener {
            val placeUpdate: placeEntity
            place.let {
                placeUpdate =
                    placeEntity(it.id, it.streetName, it.suburb, true, it.latitude, it.longitude)
            }
            upDatePlace(placeUpdate)
        }
        btn_navigate.setOnClickListener {
            val gmmIntentUri = Uri.parse("google.navigation:q=${place.latitude},${place.longitude}")
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            startActivity(mapIntent)
        }
    }

    private fun setUtils(view: View, place: placeEntity, map: GoogleMap) {
        val visited = view.findViewById<TextView>(R.id.txt_visited_details)
        val streeName = view.findViewById<TextView>(R.id.txt_name_stree_details)
        val suburb = view.findViewById<TextView>(R.id.txt_suburb_details)

        val location = LatLng(place.latitude, place.longitude)
        map.moveCamera(CameraUpdateFactory.newLatLng(location))
        map.animateCamera(CameraUpdateFactory.zoomTo(10f), 2000, null)

        if (!place.visited) {
            visited.text = "Pendiente"
            visited.setTextColor(Color.parseColor("#AAAAAA"))
            circle_card_details.setCardBackgroundColor(Color.parseColor("#AAAAAA"))
            map.addMarker(
                MarkerOptions()
                    .position(location)
                    .title("Ubicación actual")
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker))
            )

        } else {
            visited.text = "Visitada"
            visited.setTextColor(Color.parseColor("#3ac2c2"))
            btn_navigate.visibility = View.GONE
            btn_visited.visibility = View.GONE

            map.addMarker(
                MarkerOptions()
                    .position(location)
                    .title("Ubicación actual")
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_visited_marker))
            )

        }
        streeName.text = place.streetName
        suburb.text = place.suburb
    }

    private fun upDatePlace(place: placeEntity) {
        viewModel.upDatePlace(place).observe(requireActivity(), Observer {
            when (it) {
                is Resource.Loading -> {
                }
                is Resource.Success -> {
                    findNavController().navigate(R.id.action_mapsFragment_to_mainFragment)
                }
                is Resource.Failure -> {
                    Toast.makeText(requireContext(), "Error al actualizar", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        })
    }
}