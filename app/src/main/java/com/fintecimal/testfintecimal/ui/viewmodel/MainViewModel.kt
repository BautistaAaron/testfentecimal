package com.fintecimal.testfintecimal.ui.viewmodel

import androidx.lifecycle.*
import com.fintecimal.testfintecimal.data.model.placeEntity
import com.fintecimal.testfintecimal.domain.Repo
import com.fintecimal.testfintecimal.vo.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception

class MainViewModel(private val repo: Repo) : ViewModel() {

    private val listPlacesLiveData = MutableLiveData<String>()

    val fetchPlacesList = listPlacesLiveData.distinctUntilChanged().switchMap {
        liveData(Dispatchers.IO) {
            emit(Resource.Loading())
            try {
                emit(repo.getAllPlaces())
            } catch (e: Exception) {
                emit(Resource.Failure(e))
            }
        }
    }

    fun getAllPlaces() = liveData(Dispatchers.IO) {
        emit(Resource.Loading())
        try {
            emit(repo.getAllPlaces())
        } catch (e: Exception) {
            emit(Resource.Failure(e))
        }
    }

     fun savePlaces(list: List<placeEntity>)  = liveData(Dispatchers.IO) {
         emit(Resource.Loading())
         try {
             emit(repo.insertPlaces(list))
         } catch (e: Exception) {
             emit(Resource.Failure(e))
         }
    }

    fun getAllPlacesRoom() = liveData(Dispatchers.IO){
        emit(Resource.Loading())
        try {
            emit(repo.getAllPlacesRoom())
        } catch (e: Exception) {
            emit(Resource.Failure(e))
        }
    }
    fun upDatePlace(place: placeEntity) = liveData(Dispatchers.IO){
        emit(Resource.Loading())
        try {
            emit(repo.upDatePlace(place))
        } catch (e: Exception) {
            emit(Resource.Failure(e))
        }
    }
}