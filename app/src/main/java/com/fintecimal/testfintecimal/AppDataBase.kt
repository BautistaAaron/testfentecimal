package com.fintecimal.testfintecimal

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.fintecimal.testfintecimal.data.model.placeEntity
import com.fintecimal.testfintecimal.domain.PlaceDao

@Database(entities = arrayOf(placeEntity::class), version = 1)
abstract class AppDataBase : RoomDatabase() {

    abstract fun tragoDao(): PlaceDao

    companion object {
        private var INSTANCE: AppDataBase? = null
        fun getDataBase(context: Context): AppDataBase {
// if INSTANCE == NULL create a new instance if not only return the instance elvisOperator
            INSTANCE = INSTANCE ?: Room.databaseBuilder(
                context.applicationContext, AppDataBase::class.java, "places"
            ).build()
            return INSTANCE!!
        }

        fun destroyInstance() {
            INSTANCE = null
        }

    }
}