package com.fintecimal.testfintecimal.data.model

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


data class ResponseServicePlaces(
    @SerializedName("streetName")
    val streetName: String,
    @SerializedName("suburb")
    val suburb: String,
    @SerializedName("visited")
    val visited: Boolean,
    @SerializedName("location")
    val location: Location
)

data class Location(
    val latitude: Double,
    val longitude: Double
)
@Parcelize
@Entity(tableName = "places")
data class placeEntity(
    @PrimaryKey (autoGenerate = true)
    val id:Int,
    @ColumnInfo(name = "name_street")
    val streetName: String,
    @ColumnInfo(name = "suburb")
    val suburb: String,
    @ColumnInfo(name = "visited")
    val visited: Boolean,
    @ColumnInfo(name = "latitude")
    val latitude: Double,
    @ColumnInfo(name = "longitude")
    val longitude: Double
) : Parcelable