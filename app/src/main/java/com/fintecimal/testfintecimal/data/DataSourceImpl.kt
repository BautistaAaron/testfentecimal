package com.fintecimal.testfintecimal.data

import com.fintecimal.testfintecimal.AppDataBase
import com.fintecimal.testfintecimal.data.model.ResponseServicePlaces
import com.fintecimal.testfintecimal.data.model.placeEntity
import com.fintecimal.testfintecimal.domain.DataSource
import com.fintecimal.testfintecimal.vo.Resource
import com.fintecimal.testfintecimal.vo.RetrofitClient

class DataSourceImpl(private val appDataBase: AppDataBase) : DataSource {
    override suspend fun getAllPlaces(): Resource<List<ResponseServicePlaces>> {
        return Resource.Success(RetrofitClient.webservice.getAllPlaces())
    }

    override suspend fun insertPlaces(list: List<placeEntity>) : Resource<String> {
        appDataBase.tragoDao().insertAllPlaces(list)
        return Resource.Success("Exitoso")
    }

    override suspend fun getAllPlacesRoom(): Resource<List<placeEntity>> {
        return Resource.Success(appDataBase.tragoDao().getAllPlaces())
    }

    override suspend fun upDatePlace(place: placeEntity) : Resource<String> {
        appDataBase.tragoDao().upDatePlace(place)
        return  Resource.Success("")
    }
}