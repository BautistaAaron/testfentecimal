package com.fintecimal.testfintecimal.domain

import com.fintecimal.testfintecimal.data.model.ResponseServicePlaces
import retrofit2.http.GET

interface WebService {
    @GET("interview")
    suspend fun getAllPlaces() : List<ResponseServicePlaces>
}