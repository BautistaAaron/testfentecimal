package com.fintecimal.testfintecimal.domain

import com.fintecimal.testfintecimal.data.model.ResponseServicePlaces
import com.fintecimal.testfintecimal.data.model.placeEntity
import com.fintecimal.testfintecimal.vo.Resource

interface Repo {
    suspend fun getAllPlaces():Resource<List<ResponseServicePlaces>>
    suspend fun insertPlaces(listPlaces:List<placeEntity>):Resource<String>
    suspend fun getAllPlacesRoom() : Resource<List<placeEntity>>
    suspend fun upDatePlace(place: placeEntity) : Resource<String>
}