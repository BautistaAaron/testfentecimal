package com.fintecimal.testfintecimal.domain

import com.fintecimal.testfintecimal.data.model.ResponseServicePlaces
import com.fintecimal.testfintecimal.data.model.placeEntity
import com.fintecimal.testfintecimal.vo.Resource

class RepoImpl(private val dataSource: DataSource):Repo {
    override suspend fun getAllPlaces(): Resource<List<ResponseServicePlaces>> {
        return dataSource.getAllPlaces()
    }

    override suspend fun insertPlaces(listPlaces: List<placeEntity>) : Resource<String> {
        dataSource.insertPlaces(listPlaces)
        return Resource.Success("")
    }

    override suspend fun getAllPlacesRoom(): Resource<List<placeEntity>> {
        return dataSource.getAllPlacesRoom()
    }

    override suspend fun upDatePlace(place: placeEntity) : Resource<String>{
            dataSource.upDatePlace(place)
        return Resource.Success("")
    }
}