package com.fintecimal.testfintecimal.domain

import androidx.room.*
import com.fintecimal.testfintecimal.data.model.placeEntity

@Dao
interface PlaceDao {
    @Query("Select * From places")
    suspend fun getAllPlaces(): List<placeEntity>
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllPlaces(listPlaces:List<placeEntity>)
    @Update
    suspend fun upDatePlace(place:placeEntity)

}